import HomePage from "@/components/templates/HomePage";
import useStaticData from "@/hooks/useStaticData";
import React from "react";

const Home = () => {
  const { defaultData } = useStaticData();

  return <HomePage {...defaultData.homePage} />;
};

export default Home;
