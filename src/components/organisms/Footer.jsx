import { Title } from "@mantine/core";
import React, { Fragment } from "react";

const Footer = ({ title }) => {
  return <Fragment>{title && <Title>{title}</Title>}</Fragment>;
};

export default Footer;
