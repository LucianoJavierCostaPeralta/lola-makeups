import { Title } from "@mantine/core";
import React, { Fragment } from "react";

const Header = ({ title }) => {
  return <Fragment>{title && <Title>{title}</Title>}</Fragment>;
};

export default Header;
