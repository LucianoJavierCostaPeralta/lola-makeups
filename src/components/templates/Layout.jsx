import { inter } from "@/hooks/useFont";
import React from "react";
import Footer from "../organisms/Footer";
import Header from "../organisms/Header";

const Layout = ({ header, footer, children }) => {
  return (
    <>
      <Header {...header} />
      <main>{children}</main>
      <Footer {...footer} />
    </>
  );
};

export default Layout;
