import theme from "@/lib/utils/theme";
import { Text } from "@mantine/core";
import React from "react";

const HomePage = ({ title }) => {
  return (
    <Text size={20} weight="bold">
      {title}
    </Text>
  );
};

export default HomePage;
