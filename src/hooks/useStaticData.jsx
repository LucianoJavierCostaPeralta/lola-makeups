const useStaticData = () => {
  const defaultData = {
    layout: {
      header: {
        title: "Header",
      },
      footer: {
        title: "Footer",
      },
    },
    homePage: {
      title: "Lola Makeups",
    },
  };

  return {
    defaultData,
  };
};

export default useStaticData;
